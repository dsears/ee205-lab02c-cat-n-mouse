///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Dane Sears dsears@hawaii.edu
/// @date    23_JAN_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define DEFAULT_MAX_NUMBER 2048

int theMaxValue;
int aGuess, theNumberImThinkingOf;

int main( int argc, char* argv[] ) {

   //randomNumber = rand()%theMaxValue+1;
   //theMaxValue = atoi(argv[1]);
   theMaxValue = DEFAULT_MAX_NUMBER;
   if (argc == 2)
   {
      theMaxValue = atoi(argv[1]);
      if (theMaxValue < 1)
      {
         printf("INVALID INPUT: EXITING PROGRAM\n");
         return 1;
      }
   }//else if(argc == 1)
   //{
//if (argc = 1)
//{
//   theMaxValue = DEFAULT_MAX_NUMBER;
//}
   srand(time(NULL));
   theNumberImThinkingOf = rand()%theMaxValue+1;
   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n",argc);
   printf("The Max Value is %d\n", theMaxValue);

   //if( argc < 2)
//{
 //  printf( "Cat `n Mouse\n" );
   //printf( "The number of arguments is: %d\n", argc);
   //printf("DEBUG: the number is %d\n", theNumberImThinkingOf);
//
//if(argc = 1)
//{
 //   This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
printf("OK cat, I'm thinking of a random number from 1 to %d\nMake a guess:\n", theMaxValue);

   //printf( "Enter a number: " );
   scanf( "%d", &aGuess );
   //while(scanf("%d", &aGuess) != theNumberImThinkingOf)
   do
   {
   if (aGuess > theMaxValue)
   {
      printf("You must enter a number that's <= %d\n", theMaxValue);
      continue;
   }
   if (aGuess < 1)
   {
      printf("You must enter a number that's >= 1\n");
      continue;
   }
   if (aGuess < theNumberImThinkingOf)
   {
      printf("No cat... the number I'm thinking of is larger than %d\n", aGuess);
      //break;
   }
   if (aGuess > theNumberImThinkingOf)
   {
      printf("No cat... the number I'm thinking of is smaller than %d\n", aGuess);
   }
   if (aGuess == theNumberImThinkingOf)
   {
      printf("You got me.\n\n");
      printf(" /\\_/\\ \n");
      printf("( o.o )\n");
      printf(" > ^ <\n");
      return 0;
   }
   
   }while(scanf("%d", &aGuess) != theNumberImThinkingOf);
   //}
   printf( "The number was [%d]\n", theNumberImThinkingOf );
   //randomNumber = rand()%theMaxValue+1;
  // return 1;  // This is an example of how to return a 1
//}
//else
//{
   //printf("INVALID INPUT: EXITING PROGRAM\n");
  // return 1;
//}

}
